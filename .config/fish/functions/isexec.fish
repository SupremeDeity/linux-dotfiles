function isexec --description 'Checks if argument is executable.'
				if [ ! -z "$argv" ]
								echo (set_color purple) "Checking executable status..." (set_color normal)
								[ -x $argv ] && echo (set_color green) "$argv is executable" (set_color	normal)  || echo (set_color red) "$argv is not executable" (set_color normal)
				else
								echo (set_color red) "Argument is required. Syntax: isexec [argument]" (set_color normal)
				end
end
