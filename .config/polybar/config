;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #181818
background-alt = #444
foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/bar]
width = 100%
height = 25
offset-x = 0%
offset-y = 0%
radius = 6.0
;fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

padding-left = 1
padding-right = 0

module-margin-left = 1
module-margin-right = 1

font-0 = unifont:fontformat=truetype:size=8:antialias=false;0
font-1 = siji:pixelsize=10;1
font-2 = FiraCode Nerd Font Mono:pixelsize=10;2

modules-left = memory cpu filesystem
modules-center = xwindow
modules-right = pulseaudio wlan battery date powermenu

tray-position = right
tray-padding = 2
tray-background = #404040 

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = " %title:0:30:...% "
format-background = #404040
[module/filesystem]
type = internal/fs
interval = 25
fixed-values = true
mount-0 = /
format-mounted = " <label-mounted> <ramp-capacity> "
label-mounted = " %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%% "
format-mounted-background = #404040
format-mounted-underline = #9f78e1
ramp-capacity-7 = ▁
ramp-capacity-6 = ▂
ramp-capacity-5 = ▃
ramp-capacity-4 = ▄
ramp-capacity-3 = ▅
ramp-capacity-2 = ▆
ramp-capacity-1 = ▇
ramp-capacity-0 = █

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.secondary}
format-background = #404040
format-underline = #f90000
format = " <label> <bar-load> "
bar-load-indicator = ▐
bar-load-indicator-foreground = #55aa55 
bar-load-width = 10
bar-load-foreground-0 = #55aa55
bar-load-foreground-1 = #557755
bar-load-foreground-2 = #f5a70a
bar-load-foreground-3 = #ff5555
bar-load-fill = ▐
bar-load-empty = ▐
bar-load-empty-foreground = #203020 
label = " %percentage%%"

[module/memory]
interval = 1
type = internal/memory
format-prefix = "  "
format = "<label> <bar-used> "
format-underline = ${colors.secondary}
format-background = #404040
format-prefix-foreground = ${colors.secondary}
bar-used-indicator =
bar-used-width = 10
bar-used-foreground-0 = #55aa55
bar-used-foreground-1 = #557755
bar-used-foreground-2 = #f5a70a
bar-used-foreground-3 = #ff5555
bar-used-fill = ▐
bar-used-empty = ▐
bar-used-empty-foreground = #203020
[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3.0

format-connected = " <ramp-signal> <label-connected> "
format-connected-underline = #9f78e1
label-connected = %essid%
format-connected-background = #404040

format-disconnected-background = #404040
;format-disconnected =
format-disconnected = " <label-disconnected> "
format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = %ifname% disconnected
label-disconnected-foreground = ${colors.alert}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = #9f78e1

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %I:%M
time-alt = %I:%M:%S

format-prefix = " "
format-prefix-foreground = #0a6cf5
format-underline = #0a6cf5
format-background = #404040

label = "%date% %time% "

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = 🔊 Vol %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = ▐
bar-volume-indicator-font = 3
bar-volume-fill = ▐
bar-volume-fill-font = 3
bar-volume-empty = ─
bar-volume-empty-font = 3
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

format-charging = " <animation-charging> <label-charging> "
format-charging-underline = #ffb52a
format-charging-background = #404040

format-discharging = " <animation-discharging> <label-discharging> "
format-discharging-underline = ${self.format-charging-underline}
format-discharging-background = #404040

format-full-prefix = "  "
format-full-prefix-foreground = #ffb52a
format-full-underline = ${self.format-charging-underline}
format-full-background = #404040

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = #ffb52a

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.primary}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = #ffb52a
animation-discharging-framerate = 750

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open ="  "
label-open-foreground = #ffffff
label-open-background = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = systemctl reboot

menu-2-0 = power off
menu-2-0-exec = systemctl poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
