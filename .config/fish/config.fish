function fish_prompt
    powerline-shell --shell bare $status
end

function fish_greeting
    neofetch
end
